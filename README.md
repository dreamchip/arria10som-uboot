# arria10som-uboot

This is the **[Dream Chip](http://www.dreamchip.de "Dream Chip Technologies GmbH") arria10som-uboot** repository.

It contains modified uboot sources based on Altera's socfpga-v2014.10-arria10 u-boot ([Altera's Opensource u-boot repository](https://github.com/altera-opensource/u-boot-socfpga/tree/socfpga_v2014.10_arria10_bringup)) from the Quartus 17.1 Release ```2a2102e92e470beec51d8b2dea8323cfc92f92b1```.  

This u-boot version supports the **[Dream Chip Arria10 SoM](https://www.dreamchip.de/products/arria-10-system-on-module.html "The Arria 10 System on Module in detail")** with QSPI + eMMC or SD-Card boot.     

## Table of Contents

1. [Get the source code](#get_the_source)  
1.1. [Getting the latest version](#get_latest)  
1.2. [Getting a release version](#get_release)  
2. [Setup your toolchain](#setup_toolchain)  
3. [Build & Update for QSPI targets](#build_qspi)  
3.1. [Configure and compile u-boot](#build_qspi_config)  
3.2. [Create u-boot flash image](#build_qspi_create_flash)  
3.3. [Flash the QSPI u-boot image via debugger](#build_qspi_flash_target)  
4. [Build & Update for SD-Card targets](#build_sdmmc)  
4.1. [Configure and compile u-boot](#build_sdmmc_config)  
4.2. [Create u-boot flash image](#build_sdmmc_create_flash)  
4.3. [Update sd-card partition](#build_sdmmc_flash_target)  

## 1. Get the source code <a name="get_the_source"></a>

#### 1.1. Getting the latest version <a name="get_latest"></a>

    $ git clone https://gitlab.com/dreamchip/arria10som-uboot.git

#### 1.2. Getting a release version <a name="get_release"></a>

    $ git clone https://gitlab.com/dreamchip/arria10som-uboot.git -b dreamchip-arria10som-v1.0.0


## 2. Setup your toolchain <a name="setup_toolchain"></a>

    $ <path_to_toolchain>/intelFPGA/17.1/embedded/embedded_command_shell.sh
    $ export CROSS_COMPILE=arm-altera-eabi-

## 3. Build & Update for QSPI targets <a name="build_qspi"></a>

#### 3.1. Configure and compile u-boot <a name="build_qspi_config"></a>

    $ make dreamchip_arria10som_qspi_defconfig
    $ make
    

#### 3.2. Create u-boot flash image <a name="build_qspi_create_flash"></a>

    $ mkpimage --header-version 1 -o uboot_w_dtb-mkpimage.bin u-boot-dtb.bin u-boot-dtb.bin u-boot-dtb.bin u-boot-dtb.bin

#### 3.3. Flash the QSPI u-boot image via debugger <a name="build_qspi_flash_target"></a>

    $ quartus_hps --cable=1 --operation=PV --addr=0x0 uboot_w_dtb-mkpimage.bin

## 4. Build & Update for SD-Card targets <a name="build_sdmmc"></a> 
    
#### 4.1. Configure and compile u-boot <a name="build_sdmmc_config"></a>

    $ make dreamchip_arria10som_sdmmc_defconfig
    $ make

#### 4.2. Create u-boot flash image <a name="build_sdmmc_create_flash"></a>

    $ mkpimage --header-version 1 -o uboot_w_dtb-mkpimage.bin u-boot-dtb.bin u-boot-dtb.bin u-boot-dtb.bin u-boot-dtb.bin

#### 4.3. Update sd-card partition <a name="build_sdmmc_flash_target"></a>
*Note: This step needs an SD-Card which was created via the [make_sdimage.py](http://releases.rocketboards.org/release/2018.05/gsrd/tools/make_sdimage.py) tool from Altera*
	
	$ sudo dd if=uboot_w_dtb-mkpimage.bin of=/dev/sdx3 bs=64k seek=0
 