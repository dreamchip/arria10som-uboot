/*
 * (C) Copyright 2013 Altera Corporation <www.altera.com>
 *
 * SPDX-License-Identifier:	GPL-2.0+
 */

#include <common.h>
#include <malloc.h>
#include <dwmmc.h>
#include <errno.h>
#include <asm/arch/dwmmc.h>
#include <asm/arch/clock_manager.h>
#include <asm/arch/system_manager.h>
#include <fdtdec.h>

DECLARE_GLOBAL_DATA_PTR;

#define SOCFPGA_DWMMC_MIN_FREQ 400000

static const struct socfpga_clock_manager *clock_manager_base =
		(void *)SOCFPGA_CLKMGR_ADDRESS;
static const struct socfpga_system_manager *system_manager_base =
		(void *)SOCFPGA_SYSMGR_ADDRESS;

static void socfpga_dwmci_clksel(struct dwmci_host *host)
{
	unsigned int drvsel;
	unsigned int smplsel;

	/* Disable SDMMC clock. */
#if defined(CONFIG_FPGA_SOCFPGA_ARRIA10)
	clrbits_le32(&clock_manager_base->per_pll_en,
		CLKMGR_PERPLLGRP_EN_SDMMCCLK_MASK);
#else
	clrbits_le32(&clock_manager_base->per_pll.en,
		CLKMGR_PERPLLGRP_EN_SDMMCCLK_MASK);
#endif

	/* Configures drv_sel and smpl_sel */
	drvsel = CONFIG_SOCFPGA_DWMMC_DRVSEL;
	smplsel = CONFIG_SOCFPGA_DWMMC_SMPSEL;

	debug("%s: drvsel %d smplsel %d\n", __func__, drvsel, smplsel);
	writel(SYSMGR_SDMMC_CTRL_SET(smplsel, drvsel),
		&system_manager_base->sdmmcgrp_ctrl);

	debug("%s: SYSMGR_SDMMCGRP_CTRL_REG = 0x%x\n", __func__,
		readl(&system_manager_base->sdmmcgrp_ctrl));

	/* Enable SDMMC clock */
#if defined(CONFIG_FPGA_SOCFPGA_ARRIA10)
	setbits_le32(&clock_manager_base->per_pll_en,
		CLKMGR_PERPLLGRP_EN_SDMMCCLK_MASK);
#else
	setbits_le32(&clock_manager_base->per_pll.en,
		CLKMGR_PERPLLGRP_EN_SDMMCCLK_MASK);
#endif
}

int socfpga_dwmmc_init(u32 regbase, int bus_width, int index)
{
	struct dwmci_host *host;

	unsigned long clk = cm_get_mmc_controller_clk_hz();

	if (clk == 0) {
		printf("%s: MMC clock is zero!", __func__);
		return -EINVAL;
	}

	/* calloc for zero init */
	host = calloc(1, sizeof(struct dwmci_host));
	if (!host) {
		printf("%s: calloc() failed!\n", __func__);
		return -ENOMEM;
	}

	host->name = "SOCFPGA DWMMC";
	host->ioaddr = (void *)regbase;
	host->buswidth = bus_width;
	host->clksel = socfpga_dwmci_clksel;
	host->dev_index = index;
	/* fixed clock divide by 4 which due to the SDMMC wrapper */
	host->bus_hz = clk;
	host->fifoth_val = MSIZE(0x2) |
		RX_WMARK(CONFIG_SOCFPGA_DWMMC_FIFO_DEPTH / 2 - 1) |
		TX_WMARK(CONFIG_SOCFPGA_DWMMC_FIFO_DEPTH / 2);

	/*DCT::TM set f_max and f_min if they are defined in devicetree*/
	if (gd->fdt_blob != 0)
	{
		int nodeoffset;
		int len = 0;
		const int *cell;
		u32 f_max, f_min;

		nodeoffset = fdt_subnode_offset(gd->fdt_blob, 0, "sdmmc");
		if (nodeoffset >= 0) {
			cell = fdt_getprop(gd->fdt_blob, nodeoffset, "freq_limits", &len);
			if (cell && (len==2*sizeof(int))) {

				f_max = fdt32_to_cpu(cell[0]);
				f_min = fdt32_to_cpu(cell[1]);

				if (f_max > host->bus_hz || f_max < SOCFPGA_DWMMC_MIN_FREQ)
					f_max = host->bus_hz;

				if (f_min < SOCFPGA_DWMMC_MIN_FREQ || f_min > host->bus_hz)
					f_min = SOCFPGA_DWMMC_MIN_FREQ;

				if (f_min > f_max)
				{
					f_min = SOCFPGA_DWMMC_MIN_FREQ;
					f_max = host->bus_hz;
				}
				printf("%s: SDMMC freq limits set via device-tree: f_max=%d (Hz) f_min=%d (Hz)!\n", __func__,f_max,f_min);
				return add_dwmci(host, f_max, f_min);
			}
		}
	}

	return add_dwmci(host, host->bus_hz, SOCFPGA_DWMMC_MIN_FREQ);
}

